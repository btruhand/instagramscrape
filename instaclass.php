<?

class InstaScraper {
	//API base URL
	const API_URL = 'https://api.instagram.com/v1/';
	
	//Needed to get access token
	const API_GET_TOKEN_URL = 'https://api.instagram.com/oauth/access_token';

	const API_OAUTH_URL = 'https://api.instagram.com/oauth/authorize';

	//API key, API secret, API callback
	private $apiKey;
	private $apiSecret;
	private $callback;

	private $access_token;
	private $userID;
	private $username;
	private $scope = array("basic","relationships", "likes", "comments");

	public function __construct($build, $limit = 40) {
		$this->apiKey = $build['apiKey'];
		$this->apiSecret = $build['apiSecret'];
		$this->callback = $build['callback'];
		$this->limit = $limit;
	}

	private function getAPIkey() {
		return $this->apiKey;
	}

	private function getCallback() {
		return $this->callback;
	}

	private function getAPIsecret() {
		return $this->apiSecret;
	}

	public function getAccessToken() {
		return $this->access_token;
	}

	public function getUsername() {
		return $this->username;
	}

	public function getLoginURL(){
		return self::API_OAUTH_URL . '?client_id=' . $this->getAPIkey() . '&redirect_uri=' . $this->getCallback() . '&scope=' . implode('+',$this->scope) . '&response_type=code';
	}

	private function AuthorizeToken($details){
		$the_url = self::API_GET_TOKEN_URL;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $the_url);
		curl_setopt($ch, CURLOPT_POST, count($details));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($details));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$jsonData = curl_exec($ch);
		if (false == $jsonData) {
			throw new Exception("Error getting access token " . curl_error($ch));
		}

		curl_close($ch);
		return json_decode($jsonData);
	} 	
	
	private function executeCurl($url, $query_fields = array()) {
		$ch = curl_init();
		$paramStrings = null;
		if(count($query_fields) > 0) {
			$paramStrings = '&' . http_build_query($query_fields);
		}

		$fullURL = $url . $paramStrings;
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $fullURL);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$jsonData = curl_exec($ch);
		if(false == $jsonData) {
			throw new Exception("Error executing function " . curl_error($ch));
		}
		
		curl_close($ch);
		return json_decode($jsonData);
	}

	//function that handles the job of calling the API
	private function mediaFunc($function, $from = '', $to = '', $limitDisregard = false) {
		$apiURL = self::API_URL . $function . '?access_token=' . $this->getAccessToken();
		$result = array();
		$max_id = null;
		$funcLimit = $this->limit;
		$query_fields = array();
	
		if($from != '') {
			$query_fields['min_timestamp'] = (string) strtotime($from);
		}

		if($to != '') {
			$query_fields['max_timestamp'] = (string) strtotime($to);
		}
			
		while(true) {
			$decodedData = null;
			if($max_id != null) {
				$query_fields['max_id'] = $max_id;
				$decodedData = $this->executeCurl($apiURL, $query_fields);
			} else {
				$decodedData = $this->executeCurl($apiURL, $query_fields);
			}
			array_push($result, $decodedData);
			$funcLimit-= 20;
			if(!$limitDisregard) {
				if($funcLimit < 1) {
					break;
				}
			}
			if(property_exists($decodedData, 'pagination')) {
				if(property_exists($decodedData->pagination, 'next_max_id')) {
					$max_id = $decodedData->pagination->next_max_id;
				} else break;
			} else break;
		}

		return $result;
	}

	private function scrapeFunc($function) {
		$apiURL = self::API_URL . $function . '?access_token=' . $this->getAccessToken();
		return $this->executeCurl($apiURL);
	}	


	//Sets the access token provided by the Instagram API and the authenticated user's ID
	public function setTokenAndUser($code) {
		$details = array("client_id" => $this->getAPIkey(),
				 "client_secret" => $this->getAPIsecret(),
				 "grant_type" => "authorization_code",
				 "redirect_uri" => $this->getCallback(),
				 "code" => $code);

		$authorize_result = $this->AuthorizeToken($details);
		$this->userID = $authorize_result->user->id;
		$this->access_token = $authorize_result->access_token;
		$this->username = $authorize_result->user->username;
	}

	public function getUserID() {
		return $this->userID;
	}

	//Gets a user's followers
	public function getFollower($id = 'self') {
		$function = 'users/' . $id . '/followed-by';
		return $this->scrapeFunc($function);
	}

	//Gets the recent media that a user published
	public function getMedia($limitDisregard, $from  = '', $to = '', $id = 'self') {
		$function = 'users/' . $id . '/media/recent';
		return $this->mediaFunc($function, $from, $to, $limitDisregard);
	}

	
	//Gets likes for ONE media-id
	public function getLikesForMedia($id) {
		$function = 'media/' . $id . '/likes';
		return $this->scrapeFunc($function);
	}

	
	//Gets comments for ONE media-id
	public function getCommentsForMedia($id) {
		$function = 'media/' . $id . '/comments';
		return $this->scrapeFunc($function);
	}

	public function getSingleMedia($userID) {
		$function = 'users/' . $userID . '/media/recent';
		$apiURL = self::API_URL . $function . '?access_token=' . $this->getAccessToken();
		return $this->executeCurl($apiURL, array("count" => "1"));
	} 
}
