Purpose of the classes
======================

The following are the classes that are to be used in order to get an Instagram account's information such as,
particularly their followers, number of likes of their media and the number of comments on their media

Preliminary setup
-----------------

Make sure that the app has a callback URL since the application requires a user login. Currently the callback URL
is **http://myinstagram.com/GrowMint/instagram/usingclass.php** and can be seen in **config/info.php**. If the callback
wants to be changed, please change the constant *MY\_APP\_CALLBACK* to the new callback URL.

The connection to the database is established in **connectDB.php**. Changes to which database is used should be made here.
Currently the application connects to the the localhost database **instagram**.

**Note**: http://myinstagram.com/GrowMint/instagram/usingclass.php is a localhost, either make a similar localhost or a new one
and change the callback URL

Classes
=======

Collect
-------

The Collect class (accessible in collectclass.php) is the class that s responsible mainly for retrieving the information
from an Instagram user.

**Methods:**

- **\_\_construct($limit, $id, $username)**: The constructor for the Collect class. If _$limit_ (integer) is not specified then by default the class will only collect up to 40 of the user's latest media. If _$id_ and _$username_ are not both specified, then by default the class
will collect the logged in account's information. If _$id_ is given then _$username_ must also be given and vice versa or else an error
will be thrown.

**Note**: Make sure that the id and username given are actually correct. There is no checking for this so unwanted results will occur if it is not specified correctly

- getUserID(): Returns the user ID of the instagram account whose data is being collected

- getUsername(): Returns the username of the instagram account whose data is being collected

- setAllMedia(): Retrieves and stores all of the instagram account's media, even if the total media exceeds the specified limit in _$limit_

- **setMedia($limitDisregard, $from, $to)**: By default retrieves and stores the instagram account's media up to the specified limit in _$limit_. _$limitDisregard_ (boolean) needs to be given. If it is set to **true** then the limit specified in the constructor will be ignored otherwise the limit If _$from_ (string) is given then the media to be retrieved will be only those that were created after a certain timestamp as inferred from _$from_. If _$to_ (string) is given then the media to be retrieved will be only those were created before a certain timestamp as inferred from $to. Both $from and $to needs to be in DD-MM-YYYY date format or if the empty string i.e '' for the parameter to be ignored.

**NOTE:** It is important that either setAllMedia or setMedia is called first, or else no data can be collected.

- **sendComments()**: Retrieves the comments of the user's stored media and stores it in the database under the table **Comments**.

- **sendLikes()**: Retrieves the likes of the user's stored media and stores it in the database **Likes**.

- **sendFollower()**: Retrieves the Instagram account's list of followers and stores it in the database under the table **Follower**. Also stores information regarding the Instagram account's unfollowers and stores it in the database under the table **Unfollower**.

- **sendFollowerHashTag($hashtag, $limitDisregard, $from, $to)**: Both $hashtag (string) and $limitDisregard (boolean) needs to be specified. This function will find the media of the Instagram account's followers and store in the database their media that contains hashtag as specified in $hashtag in the media's caption. $limitDisregard should be set to **true** or **false** and has the same purpose as in _setMedia_. Both $from and $to serves the same purpose as in _setMedia_ but this time for the followers' media and does not need to be specified. The data is stored in the database under the table **FollowerHashtag**.

**Note:** This method uses the follower data as recorded in the database. Therefore in order to get the latest update, sendFollower should be called first.

- **sendFollowerLatestPost()**: Retrieves the instagram's account followers' latest post. Here latest post refers to the most recent media post a user made on instagram. The information is stored in the database under the table **FollowerPost**

Process
-------

The Process class (accessible in processclass.php) is responsible for processing the **instagram** database

**Methods:**

- **\_\_construct($userID, $username)**: The constructor for the Process class. Both _$userID_ and _$username_ are strings and need to be specified. They determine which instagam account's data is to be processed. Make sure both they are both valid Instagram account ID and username since there is no checking for validity.

- **processFollower($start, $end)/processComments($start,$end)/processLikes($start,$end)/processUnfollower($start,$end)**: All of these methods each process the Follower/Comments/Likes/Unfollower table. The result is the total number of followers/comments/likes/unfollowers based on the duration specified in _$start_ and _$end_, where they should be both strings in the date format DD-MM-YYYY. Result is stored in FollowerStats/CommentStats/LikeStats/CommentsStats/UnfollowerStats

**NOTE**: _$start_ and _$end_ refers to when the data was collected NOT the actual date when the comment was created or follower started following etc.

- **processEngagement($start, $end)**: Processes the Instagram account's engagement based on _$start_ and _$end_ the same way as the above process methods. As an addition this method also stores and finds all the users that have engaged with the Instagram account and stores it in the database under the **Engaged** table. Engagement is divided into three types, Follower engagement, Unfollower engagement and Unknown engagement each having their own database.

- **doAllProcess($start, $end)** - Using _$start_ and _$end_ does all the process methods as stated above except processEngagement.

- **getAllTotal()** - Gets the total followers, comments, likes, unfollowers, and engagement that are recorded so far.

Example of how to use
=====================

A full example of how to use this application can be seen in **usingclass.php**
