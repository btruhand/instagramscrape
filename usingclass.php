<?php

require_once 'instaclass.php';
require_once 'config/info.php';
require_once 'connectDB.php';
require_once 'DBchecker.php';
require_once 'processclass.php';
require_once 'collectclass.php';

$today = date("d-m-Y");
$collector = new Collect();
$collector->setAllMedia();
$collector->sendFollower();
$collector->sendComments();
$collector->sendLikes();
$collector->sendFollowerHashTag('#fanart', false);
$processor = new Process($collector->getUserID(), $collector->getUsername());
$processor->processFollower($today, $today);
$processor->getFollowerTotal($today, $today);
$processor->processComments($today, $today);
$processor->getCommentsTotal($today, $today);
$processor->processLikes($today, $today);
$processor->getLikesTotal($today, $today);
$processor->processUnfollower($today, $today);
$processor->getUnfollowerTotal($today, $today);
$processor->processEngagement($today, $today);
$processor->getEngagementTotal($today, $today);
$collector->sendFollowerLatestPost();

?>
