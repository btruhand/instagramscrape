** database

<?php

require_once 'instaclass.php';
require_once 'DBchecker.php';
require_once 'config/info.php';

class Collect {
	private $APIscraper;
	private $media;
	private $userID;
	private $username;
	private $today;	

	private function processCommenters($mediaID) {
		$result = array();
		$commentsData = $this->APIscraper->getCommentsForMedia($mediaID);
		if(property_exists($commentsData, 'data')) {
			foreach($commentsData->data as $comment) {
				if($comment->from->username != $this->username) {
					$insert = array("userID" => $comment->from->id, "Name" => $comment->from->username, "commentID" => $comment->id, "time" => $comment->created_time, "text" => $comment->text);
					$result[] = $insert;
				}
			}
		}
		return $result;
	}	
	
	private function arrayDiffEmulator($arrayCheck, $arrayAgainst) {
	
		$arrayAgainst = array_flip($arrayAgainst);
	
		foreach($arrayCheck as $key => $value) {
			if(isset($arrayAgainst[$value])) {
				unset($arrayCheck[$key]);
			}
		}

		return $arrayCheck;
	}
	
	private function processLikers($mediaID) {
		$result = array();
		$likesData = $this->APIscraper->getLikesForMedia($mediaID);
		if(property_exists($likesData, 'data')) {
			foreach($likesData->data as $like) {
				if($like->username != $this->username) {
					$insert = array("likerID" => $like->id, "Name" => $like->username); 
					$result[] = $insert;
				}
			}
		}
		return $result;
	}

	

	public function __construct($limit = 40, $id = 'self', $username = 'self') {
		$this->APIscraper = new InstaScraper(array('apiKey' => MY_APP_KEY,
				  			   'apiSecret' => MY_APP_SECRET,
				  			   'callback' => MY_APP_CALLBACK, $limit));

		$code = $_GET['code'];
		$this->APIscraper->setTokenAndUser($code);
		$this->today = date("d-m-Y");
		if(($id == 'self' && $username != 'self') || ($id != 'self' && $username == 'self')) {
			throw new Exception('Both id and username needs to be specified');
		}
		if($id === 'self') {
			$this->userID = $this->APIscraper->getUserID();
			$this->username = $this->APIscraper->getUsername();
		} else {
			$this->userID = $id;
		}

		if($id === 'self')  {
			$this->username = $this->APIscraper->getUsername();
		} else {
			$this->username = $username;
		}
	}


	private function sendCommentsData(&$media) {
		foreach($media->data as $data) {
			$mediaID = $data->id;
			$mediaURL = $data->images->standard_resolution->url;
			$caption = '';
			if(is_object($caption)) {
				$caption = $data->caption->text;
			}
			$commenterInfo = $this->processCommenters($data->id);
			
			foreach($commenterInfo as $info) {
				if(checknotinDB("Comments", array("mediaID" => $mediaID, "commentID" => $info["commentID"]))) {
					$date = date("d-m-Y", $info["time"]);
					$time = date("H:i:s", $info["time"]);
					
					$insertsql = "insert into Comments(commenterName, commenterID, text, mediaID, mediaURL, caption, userID, username, date, created_date, created_time, commentID) values('{$info["Name"]}', '{$info["userID"]}', '".mysql_real_escape_string($info["text"])."', '{$mediaID}', '{$mediaURL}', '{$caption}', '{$this->userID}', '{$this->username}', '{$this->today}', '{$date}', '{$time}', '{$info["commentID"]}')";
					mysql_query($insertsql);
				}
			}
		}
	}

	private function sendLikesData(&$media) {
		foreach($media->data as $data) {
			$mediaID = $data->id;
			$mediaURL = $data->images->standard_resolution->url;
			$caption = '';
			if(is_object($caption)) {
				$caption = $data->caption->text;
			}

			$date = date("d-m-Y", $data->created_time);
			$time = date("H:i:s", $data->created_time);
			$likerInfo = $this->processLikers($data->id);
	
			foreach($likerInfo as $info) {
				if(checknotinDB("Likes", array("mediaID" => $mediaID, "likerID" => $info["likerID"]))) {
					$insertsql = "insert into Likes(likerName, likerID, mediaID, mediaURL, caption, userID, username, date, created_date, created_time) values('{$info["Name"]}','{$info["likerID"]}', '{$mediaID}', '{$mediaURL}', '".mysql_real_escape_string($caption)."', '{$this->userID}', '{$this->username}', '{$this->today}', '{$date}', '{$time}')";
					mysql_query($insertsql);
				
				}
			}
		}
	}

	public function getUserID() {
		return $this->userID;
	}

	public function getUsername() {
		return $this->username;
	}

	public function setAllMedia() {
		$this->media = $this->APIscraper->getMedia(true, '', '', $this->userID);
	}

	public function setMedia($limitDisregard = false, $from = '', $to = '') {
		$this->media = $this->APIscraper->getMedia($limitDisregard, $from, $to, $this->userID);
	}		

	public function sendComments() {
		if(property_exists($this->media[0], 'data') && count($this->media[0]->data) > 0) {
			foreach($this->media as $paged_media) {
				$this->sendCommentsData($paged_media);
			}
		}
	}

	public function sendLikes() {
		if(property_exists($this->media[0], 'data') && count($this->media[0]->data) > 0) {
			foreach($this->media as $paged_media) {
				$this->sendLikesData($paged_media);
			}
		}
	}

	public function otherUserMedia($limitDisregard, $id, $from = '', $to = '') {
		return $this->APIscraper->getMedia($limitDisregard, $from, $to, $id);
	}

	private function sendHashTagData($userID, $hashtag, $limitDisregard, $from = '', $to = '') {
		if(!is_string($userID)) {
			throw new Exception("User ID has to be a string");
		}

		$follower_media = $this->otherUserMedia($limitDisregard, $userID, $from, $to);
		if(property_exists($follower_media[0], 'data') && count($follower_media[0]->data) > 0) { 
			foreach($follower_media as $paged_media) {
				foreach($paged_media->data as $data) {
					$byUsername = $data->user->username;
					$mediaID = $data->id;
					$caption = "";
					if(is_object($data->caption)) {
						$caption = $data->caption->text;
					}
					if(!(strpos($caption, $hashtag) === false) && checknotinDB("FollowerHashtag", array("byUsername" => $byUsername, "mediaID" => $mediaID, "username" => $this->username))) {	
						$byUserID = $data->user->id;
						$mediaURL = $data->images->standard_resolution->url;
						$created_date = date("d-m-Y", $data->created_time);
						$created_time = date("H:i:s", $data->created_time);
						$insertsql = "insert into FollowerHashtag(userID, username, byUserID, byUsername, hashtag, caption, mediaURL, mediaID, created_date, created_time, recorded_date) values('{$this->userID}', '{$this->username}', '{$byUserID}', '{$byUsername}', '{$hashtag}', '{$caption}', '{$mediaURL}', '{$mediaID}', '{$created_date}', '{$created_time}', '{$this->today}')";
						mysql_query($insertsql);
					}
				}
			}
		}
	}

	public function sendFollowerHashTag($hashtag, $limitDisregard, $from = '', $to = '') {
		$followersql = "select followerID from Follower where userID='{$this->userID}'";
		$retrieveresult = mysql_query($followersql);
		$result = array();
		while($follower = mysql_fetch_array($retrieveresult)) {
			$result[] = $follower['followerID'];
		}

		foreach($result as $followerID) {
			$this->sendHashTagData($followerID, $hashtag, $limitDisregard, $from, $to);
		}
	}
	
	public function sendFollower() {
		$followersql = "select followerName, followerID from Follower where userID='{$this->userID}'";
		$retrievefollower = mysql_query($followersql);
		
		$previous_data = array();
		
		while($data = mysql_fetch_array($retrievefollower)) {
			$previous_data[$data['followerID']] = $data['followerName'];
		}
		$new_data = array();
		$newFollower = $this->APIscraper->getFollower($this->userID);
		if(count($newFollower->data) > 0) {
			foreach($newFollower->data as $data) {
				$new_data[$data->id] = $data->username;
			}
		}

		$update_data = $this->arrayDiffEmulator($new_data, $previous_data);
		$unfollow_data = $this->arrayDiffEmulator($previous_data, $new_data);
		foreach($update_data as $data_key => $data_value) {
			if(!checknotinDB("Unfollower", array("unfollowerName" => $data_value))) {
				$deletesql = "delete from Unfollower where unfollowerName='{$data_value}'";
				mysql_query($deletesql);
			}

			$insertsql = "insert into Follower(username, userID, followerID, followerName, date) values('{$this->username}', '{$this->userID}', '{$data_key}', '{$data_value}', '{$this->today}')";
			mysql_query($insertsql);
		}

		foreach($unfollow_data as $data_key => $data_value) {
			$deletefollower = "delete from Follower where followerName='{$data_value}'";
			mysql_query($deletefollower);
			$insertunfollower = "insert into Unfollower(username, userID, unfollowerID, unfollowerName, date) values('{$this->username}', '{$this->userID}', '{$data_key}', ''{$data_value}', '{$this->today}')";
			mysql_query($insertunfollower);
		}
	}

	public function sendFollowerLatestPost() {
		$followersql = "select followerID from Follower where userID='{$this->userID}'";
		$retrievefollower = mysql_query($followersql);

		$follower_data = array();
		while($data = mysql_fetch_array($retrievefollower)) {
			$follower_data[] = $data['followerID'];
		}

		foreach($follower_data as $followerID) {
			$recentPost = $this->APIscraper->getSingleMedia($followerID);
			if(property_exists($recentPost, 'data') && count($recentPost->data) > 0) {
				$recentPost = $recentPost->data[0];
				$mediaID = $recentPost->id;
				if(checknotinDB("FollowerPost", array("userID" => $this->userID, "byUserID" => $followerID, "mediaID" => $mediaID))) {
					$mediaURL = $recentPost->images->standard_resolution->url;
					$byUsername = $recentPost->user->username;
					$caption = '';
					if(is_object($caption)) {
						$caption = $recentPost->caption->text;
					}
					$created_date = date("d-m-Y", $recentPost->created_time);
					$created_time = date("H:i:s", $recentPost->created_time);
					
					$insertsql = "insert into FollowerPost(userID, username, byUserID, byUsername, mediaID, mediaURL, caption, created_date, created_time, recorded_date) values('{$this->userID}', '{$this->username}', '{$followerID}', '{$byUsername}', '{$mediaID}', '{$mediaURL}', '{$caption}', '{$created_date}', '{$created_time}', '{$this->today}')";
					
					mysql_query($insertsql);
				}
			}
		}
	}									
}
?>
