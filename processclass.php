<?php

require_once 'connectDB.php';
require_once 'DBchecker.php';

class Process {
	
	public function __construct($userID, $username) {
		$this->userID = $userID;
		$this->username = $username;
		$this->databaselist = array("Follower", "Unfollower", "Comments", "Likes");
		$this->today = date("d-m-Y");
	}

	private function IntervalCount($table, $start, $end) {
		$type = "count";
		$tableStats = $table."Stats";
		$retrievesql = "select date from {$table} where username='{$this->username}'";
		$retrieveres = mysql_query($retrievesql);
		$data = array();
		$starttimestamp = strtotime($start);
		$endtimestamp = strtotime($end);
		while($info = mysql_fetch_array($retrieveres)) {
			$timestamp = strtotime($info['date']);
			if($starttimestamp <= $timestamp && $timestamp <= $endtimestamp) {
				$data[] = $info;
			}
		}
		
		if(!checknotinDB($tableStats, array("start" => $start, "end" => $end, "username" => $this->username))) {
			$deletesql = "delete from {$tableStats} where start='{$start}' and end='{$end}' and username='{$this->username}'";
			mysql_query($deletesql);
		}
		
		$total_in_interval = count($data);
		$totalType = "Total". $table;
		$insertsql = "insert into " . $tableStats . "(username, userID, {$totalType}, start, end, type, recorded_date) values('{$this->username}', '{$this->userID}', {$total_in_interval}, '{$start}', '{$end}', '{$type}', '{$this->today}')";
		
		mysql_query($insertsql);
	}

	private function TotalCount($table) {
		$type = "accumulate";
		$tableStats = $table."Stats";
		$TotalType = "Total".$table;
		$retrievesql = "select {$TotalType}, start, end from {$tableStats} where username='{$this->username}' and type='count'";
		if(!checknotinDB($tableStats, array("recorded_date" => $this->today, "username" => $this->username, "type" => $type))) {
			$deletesql = "delete from {$tableStats} where recorded_date='{$this->today}' and username='{$this->username}' and type='{$type}'";
			mysql_query($deletesql);
		}
		$result = mysql_query($retrievesql);
		
		$total = 0;
		while($data = mysql_fetch_array($result)) {
			if($data['start'] === $data['end']) {
				$total+= $data[$TotalType];
			}
		}

		$insertsql = "insert into " . $tableStats . "(username, userID, {$TotalType}, type, recorded_date) values('{$this->username}', '{$this->userID}', {$total}, '{$type}', '{$this->today}')";
		mysql_query($insertsql);
	}

	private function deleteEngagement($table, $start, $end) {
		return "delete from " . $table . "Engagement where start='{$start}' and end='{$end}' and username='{$this->username}'";
	}

	private function insertEngagement($table, $TotalEngagement, $start, $end) {
		return "insert into " . $table. "Engagement(userID, username, TotalEngagement, start, end, recorded_date, type) values('{$this->userID}','{$this->username}', {$TotalEngagement}, '{$start}', '{$end}', '{$this->today}', 'count')";
	}

	private function selectEngagement($engagementType) {
		return "select TotalEngagement, start, end from {$engagementType} where username='{$this->username}' and type='count'";
	}

	private function TotalEngagement($table) {
		$type = "accumulate";
		$engagementType = $table."Engagement";
		$result = mysql_query($this->selectEngagement($engagementType));

		if(!checknotinDB($engagementType, array("recorded_date" => $this->today, "username" => $this->username, "type" => $type))) {
			$deletesql = "delete from " . $engagementType . " where recorded_date='{$this->today}' and username='{$this->username}' and type='{$type}'";
			mysql_query($deletesql);
		}
		
		$total = 0;
		while($data = mysql_fetch_array($result)) {
			if($data['start'] === $data['end']) {
				$total+= $data['TotalEngagement'];
			}
		}

		$insertsql = "insert into " . $engagementType . "(username, userID, TotalEngagement, type, recorded_date) values('{$this->username}', '{$this->userID}', {$total}, '{$type}', '{$this->today}')";
		mysql_query($insertsql);
	}
	
	public function processEngagement($start, $end) {
		//getting resources
		$commentssql = "select date, commenterID, commenterName from Comments where username='{$this->username}'";
		$likessql = "select date, likerName, likerID from Likes where username='{$this->username}'";
		$retrieveComments = mysql_query($commentssql);
		$retrieveLikes = mysql_query($likessql);
		
		//getting the people
		$followsql = "select followerName from Follower where username='{$this->username}'";
		$unfollowsql = "select unfollowerName from Unfollower where username='{$this->username}'";
		$retrieveFollower = mysql_query($followsql);
		$retrieveUnfollower = mysql_query($unfollowsql); 

		$followerArray = array();
		$unfollowerArray = array();
		while($follower = mysql_fetch_array($retrieveFollower)) {
			$followerArray[] = $follower['followerName'];
		}

		while($unfollower = mysql_fetch_array($retrieveUnfollower)) {
			$unfollowerArray[] = $unfollower['unfollowerName'];
		}

		$starttimestamp = date($start);
		$endtimestamp = date($end);
		
		$engagementFollow = 0;
		$engagementUnfollow = 0;
		$engagementUnknown = 0;
		$engagedData = array();
		while($info = mysql_fetch_array($retrieveComments)) {
			$timestamp = $info['date'];
			if($starttimestamp <= $timestamp && $timestamp <= $endtimestamp) {
				if(in_array($info['commenterName'], $followerArray)) {
					$engagementFollow+= 1;
				} elseif(in_array($info['commenterName'], $unfollowerArray)) {
					$engagementUnfollow+= 1;
				} else {
					$engagementUnknown+= 1;
				}
				
				$check_insert = array("engagedName" => $info["commenterName"], "engagedID" => $info["commenterID"]);
				if(!in_array($check_insert, $engagedData)) {
					$engagedData[] = $check_insert;
				}
			}
		}


		while($info = mysql_fetch_array($retrieveLikes)) {
			$timestamp = $info['date'];
			if($starttimestamp <= $timestamp && $timestamp <= $endtimestamp) {
				if(in_array($info['likerName'], $followerArray)) {
					$engagementFollow+= 1;
				} elseif(in_array($info['likerName'], $unfollowerArray)) {
					$engagementUnfollow+= 1;
				} else {
					$engagementUnknown+= 1;
				}
				
				$check_insert = array("engagedName" => $info["likerName"], "engagedID" => $info["likerID"]);
				if(!in_array($check_insert, $engagedData)) {
					$engagedData[] = $check_insert;
				}
			}
		}

		if(!checknotinDB("FollowEngagement", array("start" => $start, "end" => $end, "username" => $this->username))) {
			mysql_query($this->deleteEngagement("Follow", $start, $end));
		}

		if(!checknotinDB("UnfollowEngagement", array("start" => $start, "end" => $end, "username" => $this->username))) {
			mysql_query($this->deleteEngagement("Unfollow", $start, $end));
		}

		if(!checknotinDB("UnknownEngagement", array("start" => $start, "end" => $end, "username" => $this->username))) {
			mysql_query($this->deleteEngagement("Unknown", $start, $end));
		}

		mysql_query($this->insertEngagement("Follow", $engagementFollow, $start, $end));
		mysql_query($this->insertEngagement("Unfollow", $engagementUnfollow, $start, $end));
		mysql_query($this->insertEngagement("Unknown", $engagementUnknown, $start, $end));

		foreach($engagedData as $insert) {
			if(checknotinDB("Engaged", $insert)) {
				$insertsql = "insert into Engaged(userID, username, engagedName, engagedID) values('{$this->userID}', '{$this->username}', '{$insert["engagedName"]}', '{$insert["engagedID"]}')";
				mysql_query($insertsql);
			}
		}
	}

	public function processFollower($start, $end) {
		$this->IntervalCount("Follower", $start, $end);
	}

	public function processComments($start, $end) {
		$this->IntervalCount("Comments", $start, $end);
	}

	public function processLikes($start, $end) {
		$this->IntervalCount("Likes", $start, $end);
	}

	public function processUnfollower($start, $end) {
		$this->IntervalCount("Unfollower", $start, $end);
	}

	public function doAllProcess($start, $end) {
		foreach($this->databaselist as $db) {
			$this->IntervalCount($db, $start, $end);
		}
	}

	private function getEngagementTotal() {
		$this->TotalEngagement("Follow");
		$this->TotalEngagement("Unfollow");
		$this->TotalEngagement("Unknown");
	}

	public function getAllTotal() {
		$this->TotalCount("Follower");
		$this->TotalCount("Comments");
		$this->TotalCount("Likes");
		$this->TotalCount("Unfollower");
		$this->TotalEngagement("Follow");
		$this->TotalEngagement("Unfollow");
		$this->TotalEngagement("Unknown");
	}
}

?>
